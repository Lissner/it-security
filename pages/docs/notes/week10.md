# Uge 10 - Udvikling af Sikker Software

## Kompileret og fortolket sprog
### Kompileret sprog bliver kompileret til en binær (.exe) fil.
* Fjerner alle metoder, variable-navne, mm. så det bliver svært at læse. / Obfuskerer source-code.

### Fortolket sprog bliver fortolket i runtime til noget vores computer kan forstå.
* Javascript
* Kan læses af alle.
* C# er i øvrigt nemt at lave tilbage til sin source-code.

## Spændende ting fra bogen
* Ansvar
* Cuss / CVE / CWS / OWASP
* (SQL) Injection
* ISO (27002 / 27005)
* CiS18
* (S) SDLC
* DEVSECOPS
* XSS (Cross Site Scripting)
* Threat modelling

### Ansvar
Alle har tildels ansvar for sikkerheden, men nogle af de primære indebærer:
* Ledelse har ansvar for retning og rammer.
* Udviklere har ansvar for design, implementering samt drift.

#### Threat modeling manifesto
1. Hvad arbejder vi på?
2. Hvad kan der gå galt?
3. Hvad har vi tænkt os at gøre ved det?
4. Gjorde vi et godt nok stykke arbejde?
Tænkt over hvad du skal bruge udviklingsmetoder og frameworks til, og evaluer om det giver mening at gøre brug af det.
Følg ikke bare blindt en hel process (eksempelvis SCRUM) uden at tænke over hvorfor.

#### Use cases og app use cases
* En use case beskriver hvordan man skal gøre noget, for at udføre en process i systemet.
* En app use case beskriver hvordan man kan bryde processen i systemet.

## Spændende ting at kigge på
### Kodeanalyse
* Sonarqube
* Gitlab (SAST / DAST)

### WAF (Web Application Firewall)
* Brugere kan foretage fejl, mindst lige så ofte.
* Black friday kan være et eksempel på et ubevidst DDoS angreb.

Jeg kan foreslå gode sikkerhedsforslag-/løsninger, men i sidste ende skal risikosbilledet opevejse hos ledelsen. Hvis trudslen er billigere for dem at udbetale, eller der eksempelvis er et forsikringsfirma hvor man kan få det gjort biligere, kan det være de ikke ønsker at bruge en masse penge på sikkerhed. - Alting bør altid opvejes.

Det er vigtigt at koge sikkerhedsprincipper ned til en kontekst man bevæger sig i. Omhandler igen ikke blindt bare at følge en model som nogen har sat op på forhånd.

### Cross Site Scripting
Omhandler i bund og grund at man ikke skal kunne afvikle udefrakommende kode igennem ens program.
Sørg for at have god sanitization når du har nogen som helst former for inputs!

### Script vs Program
* Et program er et større system der typisk er splittet op i mange filer.
* Et script er en lille fil der udfører en specialiseret opgave.