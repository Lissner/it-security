# Uge 6 - Introduktion & Linux basics

**_NOTE:_** Hele denne side blev originalt skrevet på Engelsk, og er ikke blevet oversat endnu.

Learned to connect to TryHackMe with VPN using the following command:
```sudo openvpn /home/kali/Downloads/Lissner.ovpn```

Learned about the brute force approach:
* BookFace password reset 1..10000

Learned about the attack on Walmart that cost them $300 million because the attacker got in through the ventilation system and passed as normal network traffic.

## Fundamental Linux commands

Write out a message in the console:

```echo <message>```

Display the current active user of the system:

```whoami```

List directories and files, optionally in the given directory:

```ls <directory>```

Change the current directory:

```cd <directory>```

Output the contents of a file:

```cat <file>```

Get the full path to the current working directory:

```pwd```

Automate the process of finding a specific file:

```find <flag> <file>```

Automate searching through the contents of a file:

```grep <filter>```

"Manual" lists the commands and applications available for the system. Optionally takes a command as Input:

```man <command>```

Create a file with the given name:

```touch <name>```

Create a folder/directory with the given name:

```mkdir <name>```

Remove the file or directory name given, needs -R flag to remove folder with contents:

```rm <flag> <name>```

Copy an existing file to a new file with the assigned name:

```cp <existing> <new>```

Move file to new location. Handy for renaming the files well:

```mv <file> <location>```

Determine the filetype of a file:

```file <file>```

## Operators i LinuxLearned about operators in Linux including:

Execute commands in the background:

```&```

Run commands in succession of eachother, provided the previous command succeded:

```&&```

Send the output of a command somewhere else (Overwrites contents of a file):

```>```

Send the output of a command somewhere else (Appends contents to a file):

```>>```

Learned to connect to a remote machine using SSH with the following command:

```SSH <username>@<ip>```

## Flags in Linux

"-all" as an example shows all folders, even the hidden ones when used in conjuction with ls:

```-a```

List available flags for a given command:

```--help``` - Which lists available flags for a given command.

Display the output in a human-readable way:

```-h```

Learned that files and folders with a . in front of them are hidden.
Linux files don't need extensions.

## Permissions
* "-" determines that the object is a file.
* "d" determines that the object is a directory.
* "rwd" means "read", "write" & "execute".

When typing "ls -l you're able to see the permissions in columns. The first column is the type (-/d) the second is the permission of the owner, the third is the permission of the group and the fourth it the permission for everyone. A dash instead of any in "rwd" means they don't have permission for this action.

Switching users with the "su" command. Using the -l flag stands for --login simulates and actual user login.

## Common directories
* /etc which is commonplace for system files used by the OS.
* /var stores data frequently accessed by the system.
* /root home directory for the system user.
* /tmp for temporary files.
